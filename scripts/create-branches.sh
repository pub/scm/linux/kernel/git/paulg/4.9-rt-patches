#!/bin/bash

# Create an -rt branch on top of each incremental merge in mainline by Linus.

# Assumes patches for merge "v4.x-gabcdef" are on tag "rt-v4.x-gabcdef" in
# the $PATCHES repo.

START=v4.8
END=v4.9
RC=8

# Collect path info and then clone ourselves and fork.
if [ -z "$KERNEL" ]; then
	echo "Path to existing mainline kernel repo where you want the rt bisect branches."
	echo -n "(e.g.  \"/home/paul/git/linux-rt-bisect\") Default is \"./\": "
	read KERNEL
	if [ -z "$KERNEL" ]; then
		KERNEL="./"
	fi

	KERNEL=`readlink -f $KERNEL`
	if [ ! -d "$KERNEL"/.git ]; then
		echo Entry $KERNEL does not appear to be a git repo
		exit 1
	fi

	PATCHES=`dirname $0 | sed 's/scripts/patches/'`
	PATCHES=`readlink -f $PATCHES`

	# We'll check out tags where we don't exist; so copy ourselves.
	SCRIPT=`mktemp`
	echo '#!/bin/bash' > $SCRIPT
	echo SCRIPT=$SCRIPT >> $SCRIPT
	echo KERNEL=$KERNEL >> $SCRIPT
	echo PATCHES=$PATCHES >> $SCRIPT
	cat $0 | grep -v '^#!/bin/bash' >> $SCRIPT
	chmod +x $SCRIPT
	exec $SCRIPT
fi

MERGES=`mktemp`
DMERGES=`mktemp`
MAPPING=`mktemp`
LOG=`mktemp`

echo Logging debug and errors to $LOG
# echo Mapping of merges to patch tags in $MAPPING

cd $PATCHES > $LOG 2>&1
if [ $? != 0 ]; then
	echo Can not cd into $PATCHES
	exit 1
fi

git checkout $END-rt >> $LOG 2>&1
if [ $? != 0 ];then
	echo Checkout of $END-rt in $PATCHES failed
	exit 1
fi

if [ ! -f $PATCHES/series ]; then
	echo Can not find series file in $PATCHES
	exit 1
fi

cd $KERNEL >> $LOG 2>&1
if [ $? != 0 ]; then
	echo Can not cd into $KERNEL
	exit 1
fi

# Sanity check
git rev-parse $START >> $LOG 2>&1
if [ $? != 0 ]; then
	echo Cant resolve $START in $KERNEL
	exit 1
fi
git rev-parse $END >> $LOG 2>&1
if [ $? != 0 ]; then
	echo Cant resolve $END in $KERNEL
	exit 1
fi

echo "Getting merges.."
git log --oneline --merges --author=Torvalds --reverse ^$START $END-rc1 | awk '{print $1}' > $MERGES

# post rc1 we dont do it per merge, just per rc tag.
for i in `seq $RC` ; do echo $END-rc$i >> $MERGES ; done
echo $END >> $MERGES
MCOUNT=`cat $MERGES | wc -l`

echo -n "Describing $MCOUNT merges"
for i in `cat $MERGES` ; do
	git describe $i >> $DMERGES
	echo -n .
done
echo done.

echo $START $START.0-rt > $MAPPING
PTAG=rt-START.0

cd $PATCHES
# echo "Mapping $MCOUNT merges to patch repo tags."
for i in `cat $DMERGES` ; do

	git tag | grep -q $i

	if [ $? = 0 ]; then	# patch tag match for this merge
		PTAG=rt-$i
	fi

	echo $i $PTAG >> $MAPPING
done

HERE=1
for i in `cat $DMERGES` ; do

	PTAG=`grep "^$i " $MAPPING | awk '{print $2}'`
	echo "[$HERE/$MCOUNT] Applying $PTAG to $i in $KERNEL" >> $LOG

	cd $PATCHES
	git checkout $PTAG >> $LOG 2>&1
	if [ $? != 0 ];then
		echo Checkout of $PTAG in $PATCHES failed
		exit 1
	fi

	cd $KERNEL
	git checkout -b $i-rt $i >> $LOG 2>&1
	if [ $? != 0 ];then
		echo New branch of $i-rt in $KERNEL failed
		exit
	fi

	COUNT=`cat $PATCHES/series | grep '^[a-zA-Z0-9_]' | wc -l`
	echo "[$HERE/$MCOUNT] Applying $PTAG ($COUNT patches) to $i baseline"
	for i in `cat $PATCHES/series | grep '^[a-zA-Z0-9_]'`; do
		git am $PATCHES/$i >> $LOG 2>&1
		if [ $? != 0 ]; then
			echo Git am of $PATCHES/$i failed
			exit 1
		fi
	done

	HERE=$[$HERE+1]
done

cd $PATCHES
git checkout $END-rt >> $LOG 2>&1
cd $KERNEL

# rm -f $LOG
rm -f $MERGES
rm -f $DMERGES
rm -f $MAPPING
# Finally, remove ourselves.
rm -f $SCRIPT
